CC?=cc
CFLAGS?=-O3
CFLAGS+=-Wall -g

maf: cli.o maf.o nibble.o
	$(CC) $(CFLAGS) -o $@ cli.o maf.o nibble.o

nibble.o: nibble.c nibble.h
	$(CC) $(CFLAGS) -c -o $@ nibble.c

maf.o: maf.c maf.h nibble.h
	$(CC) $(CFLAGS) -c -o $@ maf.c

cli.o: cli.c maf.h nibble.h
	$(CC) $(CFLAGS) -c -o $@ cli.c

clean:
	rm -f *.o maf
