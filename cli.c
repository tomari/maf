#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#ifdef __GNUC__ /* bswap_64 */
#include <byteswap.h>
#endif
#include "maf.h"

#define MAFVERSION "1.1"
#define BUFLEN 4096 /* multiple of 4 */
static const unsigned long SBUFLEN=3072; /* about 75% of BUFLEN */
static unsigned long long comp[BUFLEN];
static double dbl[BUFLEN];
static const unsigned short magic=0xA103;
static const unsigned short magic_rev=0x03A1;
static const unsigned short one=1;
static const unsigned char *el=(void*)&one;

static void bswap64_region(unsigned long long *p, long length);
static void usage(void);
static int compress(FILE *in, FILE *out, int flags);
static int decompress(FILE *in, FILE *out, int flags);
extern int main(int argc, char *argv[]);

#ifndef __GNUC__
static unsigned long long bswap_64(unsigned long long x);
static unsigned long long bswap_64(unsigned long long x)
{
	return (((unsigned long long)(x) << 56) |
		(((unsigned long long)(x) << 40) & 0xff000000000000ULL) |
		(((unsigned long long)(x) << 24) & 0xff0000000000ULL) |
		(((unsigned long long)(x) << 8)  & 0xff00000000ULL) |
		(((unsigned long long)(x) >> 8)  & 0xff000000ULL) |
		(((unsigned long long)(x) >> 24) & 0xff0000ULL) |
		(((unsigned long long)(x) >> 40) & 0xff00ULL) |
		((unsigned long long)(x) >> 56));
}
#endif

static void bswap64_region(unsigned long long *p, long length)
{
	long i;
	for(i=0; i+3<length; i+=4) {
		p[i]=bswap_64(p[i]);
		p[i+1]=bswap_64(p[i+1]);
		p[i+2]=bswap_64(p[i+2]);
		p[i+3]=bswap_64(p[i+3]);
	}
	for(; i<length; i++) {
		p[i]=bswap_64(p[i]);
	}
}

static void usage()
{
	static const char helpmsg[]=
		">>> MAF <<< MAkichan for Floating: FP-number compressor. v" MAFVERSION "\n"
		"[ by Hisanobu Tomari, 2009-2010. This is a public domain software. ]\n"
		"\n"
		"  Usage: maf [cd] input output\n"
		"\n"
		"     c: Compress a file\n"
		"        cb = source is in MSB-first order(big-endian)\n"
		"        cl =              LSB-first order(little-endian)\n"
		"        c  =              machine-native byte order\n"
		"\n"
		"     d: Decode a file\n"
		"        db = store the output in MSB-first order(big-endian)\n"
		"        dl =                     LSB-first order(little-endian)\n"
		"        d  =                  in the same endianness as the original\n"
		"\n"
		"    Specify `-' for STDIN/STDOUT\n";
	fputs(helpmsg,stderr);
	exit(EXIT_FAILURE);
}

static int compress(FILE *in, FILE *out, int flags)
{
	struct mafctx mc;
	size_t nread,ncompressed,nwritten;
	int status=-1,end=0;
	unsigned char bo;
	flags&=1; /* ignore automatic byteorder detection flag */
	/* write magic number */
	bo=(*el)^flags; /* source endian: big endian -> 0 little endian -> 1 */
	if((1!=fwrite(&magic,sizeof(magic),1,out)) || (1!=fwrite(&bo,sizeof(bo),1,out))) {
		status=2;
		goto emerg;
	}
	mafInitContext(&mc,comp,BUFLEN);
loop:
	nread=fread(dbl,sizeof(double),SBUFLEN,in);
	if(flags)
		bswap64_region((void*)dbl, nread);
	if(nread<SBUFLEN) {
		long i;
		for(i=nread; i<((nread+3)&(~3L)); i++) {
			dbl[i]=dbl[nread-1];
		}
		if(feof(in)) {
			status=0;
			end=1;
		} else if(ferror(in)) {
			status=1;
			end=1;
		}
	}
	ncompressed=mafCompressDouble(&mc,dbl,nread);
	if(end)
		ncompressed=mafCompressFinish(&mc);
	nwritten=fwrite(comp,sizeof(unsigned long long), ncompressed, out);
	if(ncompressed != nwritten) {
		status=2;
		goto emerg;
	}
	mafCompressRefresh(&mc);
	if(!end)
		goto loop;
emerg:
	return status;
}

static int decompress(FILE *in, FILE *out, int flags)
{
	struct mafctx mc;
	unsigned long owords;
	size_t rwords,readplan;
	int status=0,eod=0, inswap;
	unsigned long long *cmpp=comp;
	unsigned short inmagic;
	unsigned char bo;
	/* read magic and original byteorder */
	if((1!=fread(&inmagic, sizeof(magic), 1, in)) || (1!=fread(&bo,sizeof(bo),1,in))) {
		status=1;
		goto emerg;
	}
	if(inmagic==magic)
		inswap=0;
	else if(inmagic==magic_rev)
		inswap=1;
	else {
		status=3;
		goto emerg;
	}
	if(flags&2) /* automatic byteorder correction */
		flags=(*el)^bo;
	mafInitContext(&mc,comp,0);
loop:
	readplan=SBUFLEN-(cmpp-comp);
	if(readplan!=(rwords=fread(cmpp,sizeof(unsigned long long),readplan, in))) {
		/* read error handling */
		if(feof(in))
			eod=1;
		else if(ferror(in)) {
			eod=1;
			status=1;
		}
	}
	if(inswap)
		bswap64_region(cmpp,rwords);
	mafExtractRefillDone(&mc,rwords,eod);
	owords=(unsigned long)mafExtractDouble(&mc,dbl);
	if(flags) 
		bswap64_region((void*)dbl,owords);
	if(owords && owords!=fwrite(dbl,sizeof(double),owords,out)) {
		/* write error handling */
		status=2;
		goto emerg;
	}
	if(!eod) {
		cmpp=mafExtractRefillPrepare(&mc);
		goto loop;
	}
emerg:
	return status;
}


extern int main(int argc, char *argv[])
{
	FILE *in=NULL, *out=NULL;
	char *cmd, *inpath, *outpath, *q;
	int flags=0, err;
	static const char valid_cmd[]="cCdD";
	/* check validity of parameters */
	if(argc<4) usage();
	cmd=argv[1]; inpath=argv[2]; outpath=argv[3];
	if(!cmd[0] || !index(valid_cmd,cmd[0])) usage();
	if(cmd[1]=='\0') flags=2; /* automatic byteorder mode */
	else if(*el && (cmd[1]=='b' || cmd[1]=='B')) flags=1; /* enable byte swap */
	else if(!(*el) && (cmd[1]=='l' || cmd[1]=='L')) flags=1;
	/* open input and output file */
	q=inpath;
	if(inpath[0]=='-' && inpath[1]=='\0') in=stdin;
	else if(!(in=fopen(inpath,"rb"))) goto ioerr;
	q=outpath;
	if(outpath[0]=='-' && outpath[1]=='\0') out=stdout;
	else if(!(out=fopen(outpath,"wb"))) goto ioerr;
	/* dispatch */
	switch(cmd[0]) {
	case 'c':
	case 'C':
		err=compress(in,out,flags);
		break;
	case 'd':
	case 'D':
		err=decompress(in,out,flags);
		break;
	default:
		err=0;
		break;
	}
	switch(err) {
	case 1:
		q=inpath;
		goto ioerr;
	case 2:
		q=outpath;
		goto ioerr;
	case 3:
		fputs("Invalid input format\n",stderr);
		goto fail;
	}
	fclose(out);
	fclose(in);
	exit(EXIT_SUCCESS);
ioerr:
	perror(q);
fail:
	if(out) fclose(out);
	if(in)  fclose(in);
	exit(EXIT_FAILURE);
}
