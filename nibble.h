#pragma once
struct nibble_handle {
	unsigned long long *dest;
	long destlen;
	unsigned long long *pos;
	int offset;
};
extern int nibbleio_init(struct nibble_handle *nh, unsigned long long *buf, long buflen);
extern void nibbleio_appendWords(struct nibble_handle *nh, const unsigned long long *wd, const unsigned long long *endptr);
extern void nibbleio_appendNibbles(struct nibble_handle *nh, unsigned long long nibs, int start, int end);
extern unsigned long long nibbleio_readNibbles(struct nibble_handle *nh, int start, int end);
extern unsigned long long *nibbleio_readSlide(struct nibble_handle *nh);
extern void nibbleio_writeSlide(struct nibble_handle *nh);

static inline long nibbleio_wordsFloor(struct nibble_handle *nh) {
	return (nh->pos-nh->dest);
}

static inline long nibbleio_wordsCeil(struct nibble_handle *nh) {
	return (nh->pos-nh->dest)+(nh->offset?1:0);
}

static inline int nibbleio_check(struct nibble_handle *nh, int nibs) {
	return (((nh->offset + nibs) >> 4) + nh->pos) < (nh->dest+nh->destlen);
}
