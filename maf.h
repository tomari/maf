#pragma once
#include "nibble.h"
struct mafctx {
	struct nibble_handle nh;
	int histp;
	int eod;
	unsigned long long hist[64];
};

extern void mafInitContext(struct mafctx *ctx, unsigned long long *compressed, long nwords);
extern long mafCompressDouble(struct mafctx *ctx, const double *src, long nsrc);
extern long mafExtractDouble(struct mafctx *ctx, double *dst);

static inline unsigned long long *mafExtractRefillPrepare(struct mafctx *ctx)
{
	return nibbleio_readSlide(&ctx->nh);
}

static inline void mafExtractRefillDone(struct mafctx *ctx, long filledwords, int eod)
{
	if(eod) {
		ctx->nh.dest[ctx->nh.destlen+filledwords]=0ULL;
		filledwords++;
	}
	ctx->eod=eod;
	ctx->nh.destlen+=filledwords;
}

static inline void mafCompressRefresh(struct mafctx *ctx)
{
	nibbleio_writeSlide(&ctx->nh);
}

static inline long mafCompressFinish(struct mafctx *ctx)
{
	long cut=(ctx->eod)?4-(ctx->eod):0;
	nibbleio_appendNibbles(&(ctx->nh),cut,15,15);
	return nibbleio_wordsCeil(&ctx->nh);
}
