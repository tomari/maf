/* nibble I/O - implements 4-bit stream input and output
   2010 Hisanobu Tomari. This is a public domain software. */
#include "nibble.h"

extern int nibbleio_init(struct nibble_handle *nh, unsigned long long *buf, long buflen)
{
	nh->dest=buf;
	nh->destlen=buflen;
	nh->pos=buf;
	nh->offset=0;
	return 0;
}

extern void nibbleio_appendWords(struct nibble_handle *nh, const unsigned long long *wd, const unsigned long long *endptr)
{
	unsigned long long hd,tl,sr,sl;
	if(wd>endptr) return;
	sr=4*nh->offset;
	sl=64-sr;
	/* fill the first word */
	hd= *wd >> sr;
	*(nh->pos) |= hd;
	tl= (sl<64)? (*wd << sl) : 0;
	nh->pos++;
	wd++;
	/* middle */
	while(wd <= endptr) {
		hd=wd[0] >> sr;
		nh->pos[0] = tl | hd;
		tl= (sl<64)? (wd[0] << sl) : 0;
		wd++;
		nh->pos++;
	}
	/* last */
	nh->pos[0]=tl;
	return;
}

static unsigned long long nibmask(int start, int end)
{
	return ((~0ULL)>>((4*start))) & ((~0ULL)<<(4*(15-end)));
}

extern void nibbleio_appendNibbles(struct nibble_handle *nh, unsigned long long nibs, int start, int end)
{
	int niblen=end-start+1;
	unsigned long long mask,tl;
	int newoff;
	mask=nibmask(start,end);
	nibs &=mask;
	tl=(start<nh->offset)?(nibs>>(4*(nh->offset-start))):(nibs<<(4*(start-nh->offset)));
	*(nh->pos) |= tl;
	newoff=(nh->offset + niblen);
	nh->offset=newoff & 0xf;
	if(newoff>>4) {
		nh->pos++;
		*nh->pos=nh->offset?nibs<<4*(16-nh->offset):0ULL;
	}
}

extern unsigned long long nibbleio_readNibbles(struct nibble_handle *nh, int start, int end)
{
	unsigned long long result=0,mask,tmp;
	int nextoff;
	if(end-start>=15-nh->offset) {
		mask=nibmask(nh->offset,15);
		result=((*nh->pos)&mask)<<(4*(nh->offset-start));
		nh->pos++;
		start+=16-nh->offset;
		nh->offset=0;
	}
	if(end<start) goto quit;
	nextoff=nh->offset+end-start+1;
	mask=nibmask(nh->offset,nextoff);
	tmp=(*nh->pos)&mask;
	result |= (start<nh->offset)?(tmp<<(4*(nh->offset-start))):(tmp>>(4*(start-nh->offset)));
	nh->offset=nextoff;
quit:
	return result;
}

extern unsigned long long *nibbleio_readSlide(struct nibble_handle *nh)
{
	long ncopy=nh->destlen-nibbleio_wordsFloor(nh);
	long i;
	for(i=0; i<ncopy; i++)
		nh->dest[i]=nh->pos[i];
	nh->pos=nh->dest;
	nh->destlen=ncopy;
	return &nh->dest[ncopy];
}

extern void nibbleio_writeSlide(struct nibble_handle *nh)
{
	nh->dest[0]=nh->pos[0];
	nh->pos=nh->dest;
	return;
}

