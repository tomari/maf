/* MAkichan for Floating. Floating Point Number Compressor
   Version 2 - 2010 Hisanobu Tomari. This is a public domain software. */
#include "maf.h"
/* .rodata */
static const unsigned int mapaddr[15]={0, 1, 2, 3, 4, 5, 6, 7,
				       31, 32, 33, 34, 56, 57, 63};
static const unsigned long long MASK_SGNEXP=0xFFF0000000000000ULL;
static const unsigned int MASK_HISTP=0x3f;

/* .text */
extern void mafInitContext(struct mafctx *ctx, unsigned long long *compressed, long nwords)
{
	unsigned int i;
	nibbleio_init(&(ctx->nh),compressed,nwords);
	ctx->histp=0;
	ctx->eod=0;
	for(i=0; i<64; i++)
		ctx->hist[i]=0x3FF3FF3FF3FF0000ULL;
	return;
}

static inline unsigned long long pack_exp(unsigned long long v0, unsigned long long v1,
					  unsigned long long v2, unsigned long long v3)
{
	unsigned long long packed=(v0&MASK_SGNEXP) | ((v1&MASK_SGNEXP)>>12) |
		((v2&MASK_SGNEXP)>>24) | ((v3&MASK_SGNEXP)>>36);
	return packed;
}

static inline void unpack_exp(unsigned long long packed, int *e)
{
	static const unsigned int MASK_LSB12=0xfff;
	e[0]=packed>>52;
	e[1]=(packed>>40)&MASK_LSB12;
	e[2]=(packed>>28)&MASK_LSB12;
	e[3]=(packed>>16)&MASK_LSB12;
}

static inline unsigned long long histbuf_ref(struct mafctx *ctx, unsigned int laddr)
{
	return ctx->hist[(ctx->histp-mapaddr[laddr])&MASK_HISTP];
}

extern long mafCompressDouble(struct mafctx *ctx, const double *src, long nsrc)
{
	unsigned int i,j,k;
	long rlen=(nsrc+3)&(~0x3L);
	for(i=0; i+3<rlen; i+=4) {
		unsigned long long expv;
		const unsigned long long *vsrc=(unsigned long long *)(&src[i]);
		int sgnexp[4];
		expv=pack_exp(vsrc[0],vsrc[1],vsrc[2],vsrc[3]);
		/* look for match */
		for(j=0; j<15; j++) {
			if(expv==histbuf_ref(ctx,j)) {
				nibbleio_appendNibbles(&(ctx->nh),j,15,15);
				for(k=0; k<4; k++)
					nibbleio_appendNibbles(&(ctx->nh),vsrc[k],3,15);
				goto update_hist;
			}
		}
		/* look for near match */
		for(j=0; j<4; j++)
			sgnexp[j]=(int) ((vsrc[j]&MASK_SGNEXP)>>52);
		for(j=0; j<15; j++) {
			unsigned long long h=histbuf_ref(ctx,j);
			int e[4],d[4];
			unpack_exp(h,e);
			for(k=0; k<4; k++) d[k]=sgnexp[k]-e[k];
			if( -8 <= d[0] && d[0] <= 7 && -8 <= d[1] && d[1] <= 7 &&
			    -8 <= d[2] && d[2] <= 7 && -8 <= d[3] && d[3] <= 7 ) {
				unsigned long long dv=((d[0]&0xf)<<12)|((d[1]&0xf)<<8)|
					((d[2]&0xf)<<4)|(d[3]&0xf);
				nibbleio_appendNibbles(&(ctx->nh),0xf00000|(j<<16)|dv,10,15);
				for(k=0; k<4; k++)
					nibbleio_appendNibbles(&(ctx->nh),vsrc[k],3,15);
				goto update_hist;
			}
		}
		/* cannot compress (no match) */
		nibbleio_appendNibbles(&(ctx->nh),0xff,14,15);
		nibbleio_appendWords(&(ctx->nh),vsrc,&vsrc[3]);
	update_hist:
		ctx->histp=(ctx->histp+1)&MASK_HISTP;
		ctx->hist[ctx->histp]=expv;
	}
	ctx->eod=nsrc&0x3;
	return nibbleio_wordsFloor(&ctx->nh);
}

static inline signed int signext_nibble(unsigned int nibble_at_lsb)
{
	signed int i=(signed int) (nibble_at_lsb << 28);
	signed int res=i>>28;
	return res;
}

extern long mafExtractDouble(struct mafctx *ctx, double *dst)
{
	unsigned long long *rdst=(unsigned long long*)dst;
	unsigned long long *pos=rdst;
	unsigned int i;
	while(nibbleio_check(&ctx->nh,264/4)) {
		unsigned long long expv;
		unsigned int head=(unsigned int) nibbleio_readNibbles(&ctx->nh, 15,15);
		if(head<0xf) { /* format 1 */
			/* read exponents */
			expv=histbuf_ref(ctx,head);
			/* read 52*4 bits */
			for(i=0; i<4; i++) {
				unsigned long long exp= (expv << (12*i)) &MASK_SGNEXP ;
				pos[i]=exp | nibbleio_readNibbles(&ctx->nh, 3,15);
			}
			goto update_hist;
		}
		head=(unsigned int) nibbleio_readNibbles(&ctx->nh, 15,15);
		if(head<0xf) { /* format 2 */
			/* read 16 bits */
			unsigned int diffv=(unsigned int)nibbleio_readNibbles(&ctx->nh,12,15);
			unsigned long long expbasev=histbuf_ref(ctx,head);
			int expbase[4];
			unpack_exp(expbasev,expbase);
			/* read 52*4 bits */
			for(i=0; i<4; i++) {
				signed int diff=signext_nibble(diffv>>(12-4*i));
				unsigned long long pexp=((unsigned long long)(expbase[i]+diff))<<52;
				pos[i]=pexp|nibbleio_readNibbles(&ctx->nh,3,15);
			}
			goto set_expv_and_update;
		}
		
		/* format 3 */
		/* read 256 bits */
		for(i=0; i<4; i++)
			pos[i]=nibbleio_readNibbles(&ctx->nh,0,15);
	set_expv_and_update:
		expv=pack_exp(pos[0],pos[1],pos[2],pos[3]);
	update_hist:
		pos+=4;
		ctx->histp=(ctx->histp+1)&MASK_HISTP;
		ctx->hist[ctx->histp]=expv;
	}
	if(ctx->eod) {
		long cut=nibbleio_readNibbles(&ctx->nh,15,15);
		return pos-rdst-cut;
	} else
		return pos-rdst;
}

